import { AppRegistry, View } from 'react-native';
import React from 'react';
import Header from './src/components/header';
import AlbumList from './src/components/AlbumList';

const App = () => (
  <View style={{ flex: 1, backgroundColor: '#636363' }}>
    <Header headerText={'Preview Music'} />
    <AlbumList />
  </View>
);

AppRegistry.registerComponent('albums', () => App);
