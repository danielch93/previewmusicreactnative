import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';

class ButtonLike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artistId: props.artistId,
      name: props.nameButtonLike,
      like: props.like
    };
  }

  onPressBtn(artistId) {
      axios.get(`http://192.168.0.21:3004/api/artist/${artistId}`)
      .then(res => {
        const like = res.data.artist.like + 1;
        axios.put(`http://192.168.0.21:3004/api/artist/${artistId}`, {
          like
        }).then(() => {
          axios.get(`http://192.168.0.21:3004/api/artist/${artistId}`)
          .then(resLike => {
            this.setState({ like: resLike.data.artist.like });
          }).catch((error) => {
            console.log(error);
          });
        });
      }
    );
  }

  render() {
    return (
      <TouchableOpacity
        onPress={() => this.onPressBtn(this.state.artistId)}
        style={styles.buttonStyle}
      >
        <View style={styles.groupTextStyle}>
          <Icon name="heart" size={20} color="#fff" style={styles.heart} />
          <Text style={styles.textStyle}>
            {this.state.like}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = {
  heart: {
    marginTop: 11
  },

  textStyle: {
    color: '#fff',
    fontSize: 20,
    fontWeight: '600',
    padding: 10
  },

  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#FF4863',
    borderWidth: 1,
    borderColor: '#fff',
    marginLeft: 2,
    marginRight: 2,
    borderRadius: 5
  },

  groupTextStyle: {
    alignSelf: 'center',
    flexDirection: 'row',
  }
};

export default ButtonLike;
