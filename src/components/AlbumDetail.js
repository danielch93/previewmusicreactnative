import React from 'react';
import { Text, View, Image, Linking } from 'react-native';

import Card from './Card';
import Button from './Button';
import Comments from './Comments';
import ButtonLike from './ButtonLike';
import CardSection from './CardSection';
import ButtonComment from './ButtonComment';
import Icon from 'react-native-vector-icons/FontAwesome';

const AlbumDetail = ({ album }) => {
  const { preview, picture, song, name, cover, _id, like } = album;
  const {
    imageStyle,
    thumbnailStyle,
    headerTextStyle,
    headerContentStyle,
    thumbnailContainerStyle
  } = styles;

  return (
    <Card>
      <CardSection>
        <View style={thumbnailContainerStyle}>
          <Image
            style={thumbnailStyle}
            source={{ uri: picture }}
          />
        </View>
        <View style={headerContentStyle}>
          <Text style={headerTextStyle}>{song}</Text>
          <Text>{name}</Text>
        </View>
      </CardSection>

      <CardSection>
        <Image
          style={imageStyle}
          source={{ uri: cover }}
        />
      </CardSection>

      <CardSection>
        <Button onPress={() => Linking.openURL(preview)}>
          <Icon name="play" size={30} color="#FFFFFF" />
        </Button>
        <ButtonLike
          nameButtonLike={'Me gusta'}
          artistId={_id}
          like={like}
        />
      </CardSection>

      <CardSection>
        <Comments artistId={_id} />
      </CardSection>

      <CardSection>
        <ButtonComment artistId={_id} />
      </CardSection>
    </Card>
  );
};

const styles = {
  headerContentStyle: {
      flexDirection: 'column',
      justifyContent: 'space-around'
  },

  headerTextStyle: {
    fontSize: 18
  },

  thumbnailStyle: {
    height: 50,
    width: 50
  },

  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },

  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  }
};

export default AlbumDetail;
