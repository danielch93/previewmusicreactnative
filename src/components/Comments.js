import React, { Component } from 'react';
import { TextInput, View, TouchableOpacity, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';

class Comments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      placeholder: 'Deja un comentario',
      artistId: props.artistId,
      comment: ''
    };
  }

  handleComment = (value) => {
    this.setState({ comment: value });
  }

  catchComment = (comment, artistId) => {
    if (comment != '') {
      axios.post('http://192.168.0.21:3004/api/comment', {
        artistId,
        comment
      }).then(() => {
      }).catch((error) => {
        console.log(error);
      });
    } else {
      return false;
    }
  }

  render() {
    return (
      <View style={styles.commentStyle}>
        <TextInput
          style={styles.inputStyle}
          placeholder={this.state.placeholder}
          onChangeText={this.handleComment}
        />
        <TouchableOpacity
          style={styles.buttonStyle}
          onPress={() => this.catchComment(this.state.comment, this.state.artistId)}
        >
          <Icon name="send" size={20} color="#FFFFFF" style={styles.sendStyle}/>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  sendStyle: {
    alignSelf: 'center',
    padding: 13,
    marginRight: 5
  },
  commentStyle: {
    flex: 1,
    flexDirection: 'row',
    margin: 3
  },

  inputStyle: {
    color: 'grey',
    width: 270
  },

  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 15,
    fontWeight: '600',
    padding: 10
  },

  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#007aff',
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 100
  }
};

export default Comments;
