import React, { Component } from 'react';
import { Text, TouchableOpacity, View, FlatList } from 'react-native';
import Modal from 'react-native-modal';
import axios from 'axios';

class ButtonComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      comment: '',
      placeholder: 'Deja un comentario',
      artistId: props.artistId,
      isModalVisible: false
    };
  }

  onPressBtn(artistId) {
    this.setState({ isModalVisible: !this.state.isModalVisible });
    axios.get(`http://192.168.0.21:3004/api/comment/${artistId}`)
    .then(response => {
      if (response.data.comment == '') {
        response.data.comment.push({ comment: 'Aun no tiene comentarios' });
        this.setState({ comment: response.data.comment });
      } else {
        this.setState({ comment: response.data.comment });
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  exitModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  }

  render() {
    return (
      <View style={styles.listStyle}>
        <View>
          <TouchableOpacity
            style={styles.buttonStyle}
            onPress={() => this.onPressBtn(this.state.artistId)}
          >
            <Text style={styles.textStyle}>Ver comentarios</Text>
          </TouchableOpacity>
          <Modal isVisible={this.state.isModalVisible}>
            <View style={styles.modalStyle}>
            <Text style={styles.TextHeaderModal}>Comentarios de las canciones</Text>
              <View style={styles.viewListStyle}>
                <FlatList
                  data={this.state.comment}
                  renderItem={
                    ({ item }) =>
                    <Text style={styles.TextItemComment}>
                      {item.comment}
                    </Text>
                  }
                  keyExtractor={(item, index) => index}
                />
              </View>
              <TouchableOpacity onPress={() => this.exitModal()}>
                <Text style={styles.TextExitModal}>Volver</Text>
              </TouchableOpacity>
            </View>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = {
  viewListStyle: {
    height: 250
  },

  modalStyle: {
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: '#fff',
    borderColor: '#fff'
  },

  textModal: {
      color: '#fff'
  },

  TextExitModal: {
    alignSelf: 'center',
    color: '#FFA200',
    fontSize: 23,
    fontWeight: '600',
    padding: 10
  },
  TextHeaderModal: {
    alignSelf: 'center',
    color: '#007AFF',
    fontSize: 25,
    fontWeight: '600',
    padding: 5,
  },

  TextItemComment: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    color: '#575757',
    fontSize: 20,
    fontWeight: '400',
    padding: 3,
    borderBottomColor: '#D6D6D6',
    borderBottomWidth: 1
  },

  textStyle: {
    alignSelf: 'center',
    color: '#fff',
    fontSize: 15,
    fontWeight: '600',
    padding: 10
  },

  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#FFA200',
    borderWidth: 1,
    borderColor: '#fff',
    marginLeft: 5,
    marginRight: 5,
    borderRadius: 5
  },

  buttonHideStyle: {
    width: 40,
    backgroundColor: '#FFA200',
    borderWidth: 1,
    borderColor: '#fff'
  },

  item: {
    margin: 5,
    padding: 10,
    fontSize: 18,
    height: 44,
    borderWidth: 1,
    borderColor: '#D1D1D1',
    borderRadius: 5
  },

  listStyle: {
    flex: 1
  }
};

export default ButtonComment;
